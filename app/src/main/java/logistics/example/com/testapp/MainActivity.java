package logistics.example.com.testapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private String originalString;
    TextView mJsonDataTextView, mJavaStatus, mAndroidStatus, mIosStatus, mNameDataTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mJsonDataTextView = (TextView) findViewById(R.id.original_data_text_view);
        mNameDataTextView = (TextView) findViewById(R.id.name_text_view);
        mIosStatus = (TextView) findViewById(R.id.ios_text_view);
        mAndroidStatus = (TextView) findViewById(R.id.android_text_view);
        mJavaStatus = (TextView) findViewById(R.id.is_java_programmer);
        originalString = "{\"name\": \"Krishna\", \"programmer\": {\"JAVA\": true, \"Android\": true, \"iOS\": false}}";
        mJsonDataTextView.setText(originalString);
        parserData();
    }

    private void parserData() {
        parseJsonData(originalString);
    }

    private void parseJsonData(String data) {

        JSONTokenizer tokenizer = new JSONTokenizer(data);
        if (tokenizer.currentTokenType().equals(JSONTokenizer.TokenType.START_BRACE)) {
            String jToken = tokenizer.getToken();
            printLog(jToken);
        }
        if (tokenizer.nextToken().equals(JSONTokenizer.TokenType.COLON)) {
            String nameKey = tokenizer.getToken();
            printLog(nameKey);
        }
        if (tokenizer.nextToken().equals(JSONTokenizer.TokenType.COMMA)) {
            String nameValue = tokenizer.getToken();
            mNameDataTextView.setText(nameValue);
            printLog(nameValue);
        }
        if (tokenizer.nextToken().equals(JSONTokenizer.TokenType.COLON)) {
            String programmer = tokenizer.getToken();
            printLog(programmer);
        }

        if (tokenizer.nextToken().equals(JSONTokenizer.TokenType.START_BRACE)) {
            String programmerStatus = tokenizer.getToken();
            printLog(programmerStatus);
        }

        if (tokenizer.nextToken().equals(JSONTokenizer.TokenType.COLON)) {
            String java = tokenizer.getToken();
            printLog(java);
        }

        if (tokenizer.nextToken().equals(JSONTokenizer.TokenType.COMMA)) {
            String javaStatus = tokenizer.getToken();
            mJavaStatus.setText(javaStatus);
            printLog(javaStatus);
        }
        if (tokenizer.nextToken().equals(JSONTokenizer.TokenType.COLON)) {
            String android = tokenizer.getToken();
            printLog(android);
        }
        if (tokenizer.nextToken().equals(JSONTokenizer.TokenType.COMMA)) {
            String androidStatus = tokenizer.getToken();
            mAndroidStatus.setText(androidStatus);
            printLog(androidStatus);
        }
        if (tokenizer.nextToken().equals(JSONTokenizer.TokenType.COLON)) {
            String ios = tokenizer.getToken();
            printLog(ios);
        }
        if (tokenizer.nextToken().equals(JSONTokenizer.TokenType.END_BRACE)) {
            String iosStatus = tokenizer.getToken();
            mIosStatus.setText(iosStatus);
            printLog(iosStatus);
        }
    }

    private void printLog(String logData) {
        Log.d("log tag-", logData);
    }
}
